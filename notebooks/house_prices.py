# %%
import matplotlib.pyplot as plt
import numpy as np
from sklearn.datasets import load_boston
from sklearn.linear_model import LinearRegression

# %%
boston_dataset = load_boston()
target_index = 0
rm_index = 5
print(boston_dataset.DESCR)

# %%
X, y = (boston_dataset.data, boston_dataset.target)
y = np.reshape(y, (-1, 1)) # Convert 1D (of N samples) to 2D (of N_samples x 1)
print(f'X shape --> {X.shape}')
print(f'y shape --> {y.shape}')

# %%
# Obtain the set division (70/30) length
train_length = round(X.shape[0] * 0.7)
test_length = X.shape[0] - train_length
print(f'Train length --> {train_length}')
print(f'Test length --> {test_length}')

# %%
# Create the two sets, for inputs "X" and targets "y"
X_train = X[0:train_length, ]
X_test = X[train_length-1:-1, ]
y_train = y[0:train_length, ]
y_test = y[train_length-1:-1, ]
print('Training set:')
print(f'\tX shape --> {X_train.shape}')
print(f'\ty shape --> {y_train.shape}')
print('Test set:')
print(f'\tX shape --> {X_test.shape}')
print(f'\ty shape --> {y_test.shape}')


# %%
def plot_data_xy(x, y, title, theta=None):
    plt.scatter(x, y)
    plt.title(title)
    plt.xlabel('x')
    plt.ylabel('y')
    if theta is not None:
        plt.plot(x, x * theta[1] + theta[0], color='red')
    plt.show()


# %%
# Plot full data set
plot_data_xy(X[:,rm_index], y[:,target_index], 'Data set')

# %%
# Plot training set
plot_data_xy(X_train[:,rm_index], y_train[:,target_index], 'Training set')

# %%
# Plot test set
plot_data_xy(X_test[:,rm_index], y_test[:,target_index], 'Test set')
# We see a data mismatch, we can address it by retrieving more real data or sitetizing new data.

# %%
# For the shake of the example I will cheat and just merge a bit of data from the training set (don't do it in real projects!!!)
chunk_of_X_train = X_train[0:int(train_length*0.2), ]
chunk_of_y_train = y_train[0:int(train_length*0.2), ]
X_test = np.concatenate((X_test, chunk_of_X_train))
y_test = np.concatenate((y_test, chunk_of_y_train))
print('Training set:')
print(f'\tX shape --> {X_train.shape}')
print(f'\ty shape --> {y_train.shape}')
print('Test set:')
print(f'\tX shape --> {X_test.shape}')
print(f'\ty shape --> {y_test.shape}')

# %%
# Plot test set
plot_data_xy(X_test[:,rm_index], y_test[:,target_index], 'New test set')
# We still see some outliers we could get rid with other ML methods, but note real data can have also those outliers,
#the main idea is to know our data and make coherent predictions

# %%
regression = LinearRegression().fit(X_train[:, rm_index:rm_index+1], y_train)
theta_sklearn = []
theta_sklearn.append(regression.intercept_)
theta_sklearn.append(regression.coef_[0])

# %%
print('Parameters obtained with sklearn Linear Model')
print(f'\tTheta sub 0 (bias parameter) --> {theta_sklearn[0][0]}')
print(f'\tTheta sub 1 (feature coefficient) --> {theta_sklearn[1][0]}')
plot_data_xy(X_train[:,rm_index], y_train[:,target_index], 'Training set (sklearn)', theta_sklearn)

# %%
y_hat_sklearn = regression.predict(X_train[:, rm_index:rm_index+1])
print(f'Prediction result example: y={y_train[100,0]} | y_hat={y_hat_sklearn[100,0]}')
